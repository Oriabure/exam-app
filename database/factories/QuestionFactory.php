<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $categoryId = mt_rand(1,3);
        return [
          'question' =>  $this->faker->sentence($nbWords = 6, $variableNbWords = true) ,
          'category_id' => $categoryId,
        ];
    }
}
