<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\QuestionCategory;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['technical', 'aptitude', 'logical'];

        foreach ($categories as $key => $category) {
            QuestionCategory::firstOrCreate([
              'category_name' => $category
            ]);
        }
    }
}
