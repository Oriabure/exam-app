<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionCategory;

class QuestionController extends Controller
{
    public function index()
    {
        return view('question.index');
    }

    public function create()
    {
        $categories = QuestionCategory::all();
        return view('question.create', compact('categories'));
    }
}
