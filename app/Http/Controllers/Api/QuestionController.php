<?php

namespace App\Http\Controllers\Api;

use App\Enums\StatusCodes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Questions\QuestionInterface;

class QuestionController extends Controller
{
    public $questionInterface;

    public function __construct(QuestionInterface $questionInterface)
    {
        $this->questionInterface = $questionInterface;
    }

    public function all(Request $request)
    {
        $questions = $this->questionInterface->all($request);
        return response()->json([
          'code' => StatusCodes::OK,
          'questions' => $questions,
          'msg' => 'Successful'
        ]);
    }

    public function index()
    {
        return view('question.index');
    }
}
