<?php

namespace App\Enums;


class StatusCodes
{
  const OK = 200;
  const CREATED = 201;
  const UPDATED = 202;
  const VALIDATION = 422;
  const NOT_FOUND = 404;
  const BAD_REQUEST = 400;
  const SERVER_ERROR = 500;
  const FORBIDDEN = 403;
}
