<?php

namespace App\Repositories\Questions;

interface QuestionInterface
{
    public function create($request);
    public function edit($request, $questionId);
    public function findById($questionId);
    public function delete($questionId);
    public function all($request);
}
