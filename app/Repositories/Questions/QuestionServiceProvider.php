<?php
namespace App\Repositories\Questions;

use Illuminate\Support\ServiceProvider;

class QuestionServiceProvider extends ServiceProvider
{
    /**
     *  Register AccountInfoServices
     * @return  void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Questions\QuestionInterface',
                        'App\Repositories\Questions\QuestionRepository');
    }
}
