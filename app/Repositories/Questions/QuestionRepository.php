<?php
namespace App\Repositories\Questions;

use App\Models\Question;
use App\Repositories\Questions\QuestionInterface;

class QuestionRepository implements QuestionInterface
{
     public function create($request)
     {
          $question = new Question;
          $this->setQuestionProperties($request, $question);
          $question->save();
     }

     public function edit($request, $questionId)
     {
         $question = $this->findById($questionId);
         $this->setQuestionProperties($request, $question);
         $question->save();
     }

     private function setQuestionProperties($request, $question)
     {
        $question->question = $request->question_name;
        $question->category_id = $request->category_id;
        return $question;
     }

     public function findById($questionId)
     {
        return Question::find($questionId);
     }

     public function delete($questionId)
     {
        $question = $this->findById($questionId);
        $question->delete();
        return true;
     }

     public function all($request)
     {
        return Question::with('category')->get();
     }
}
