<?php

namespace App\Repositories\Option;

interface OptionInterface
{
    public function create($request);
    public function edit($request, $optionId);
    public function findById($optionId);
    public function delete($optionId);
    public function all($request);
}
