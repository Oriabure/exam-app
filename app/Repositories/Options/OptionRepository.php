<?php
namespace App\Repositories\Options;

use App\Models\Option;


class OptionRepository implements OptionInterface
{
     public function create($request)
     {
          $option = new Option;
          $this->setOptionProperties($request, $option);
          $option->save();
     }

     public function edit($request, $optionId)
     {
         $option = $this->findById($optionId);
         $this->setOptionProperties($request, $option);
         $option->save();
     }

     private function setOptionProperties($request, $option)
     {
        $option->option = $request->option;
        $option->is_correct = $request->is_correct;
        $option->question_id = $request->question_id;
        return $option;
     }

     public function findById($optionId)
     {
        return Option::find($optionId);
     }

     public function delete($optionId)
     {
        $option = $this->findById($optionId);
        $option->delete();
        return true;
     }

     public function all()
     {
        return Option::all();
     }
}
