<?php


namespace App\Repositories\Options;


use Illuminate\Support\ServiceProvider;

class OptionServiceProvider extends ServiceProvider
{
    /**
     *  Register AccountInfoServices
     * @return  void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Options\OptionInterface',
                        'App\Repositories\Options\OptionRepository');
    }
}
