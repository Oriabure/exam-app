<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Exam App</title>


  <link rel="icon" href="/favicon.ico">
  <link href="/css/app.css" rel="stylesheet">
  <!-- <link rel="stylesheet" href="css/styles.css?v=1.0"> -->

</head>

<body>
  @yield('content')
  <!-- your content here... -->
    <script src="/js/app.js"></script>
</body>
</html>
