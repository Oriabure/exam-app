@extends('base')

@section('content')

  <div class="container">
      <!-- This is content -->
      <div id="app">
        <form-component></form-component>
      </div>
  </div>

@endsection
