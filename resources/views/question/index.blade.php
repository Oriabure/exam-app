@extends('base')

@section('content')

  <div class="container">
      <!-- This is content -->
      <div id="app">
        <question-component></question-component>
      </div>
  </div>

@endsection
